﻿namespace WebApiTest.Models
{
    public class Directory
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}