﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTest.Models
{
    public static class DirectoryManager
    {
        static readonly int smallFile = 10485760;
        static readonly int mediumFile = 52428800;
        static readonly int largeFile = 104857600;

        public static string[] Size = { "Small", "Medium", "Large", "Largest" };

        public static Task<DirectoryInfos> Disks()
        {
            var drives = from drive in DriveInfo.GetDrives()
                         select new Directory
                         {
                             Name = drive.Name,
                             Path = drive.Name
                         };
            DirectoryInfos dirInfo = new DirectoryInfos
            {
                Path = "",
                DirectoriesList = drives.ToList()

            };
            return Task.Run(() => dirInfo);
        }
        public static Task<DirectoryInfos> DriveInfos(string path)
        {
            var drInfo = new DirectoryInfo(path);          
            if (!drInfo.Exists)
                return null;
            var files = drInfo.GetFiles();
            var directories = drInfo.GetDirectories();

            var counts = files.GroupBy(s => s.Length < smallFile ? Size[0] :
                    s.Length < mediumFile ? Size[1] :
                    s.Length < largeFile ? Size[2] :
                    Size[3]
            )
            .ToDictionary(
                group => group.Key,
                group => group.Count()
            );
            var directory = new DirectoryInfos()
            {

                FilesList = (from file in files select new Directory { Name = file.Name, Path = file.FullName }).ToList(),
                DirectoriesList = (from file in directories select new Directory { Name = file.Name, Path = file.FullName }).ToList()
            };
            directory.DirectoriesList.Insert(0, new Directory { Name = ".....", Path = drInfo.Parent?.FullName });
            directory.Path = path;

            if (counts.ContainsKey(Size[0]))
                directory.SmallFiles = counts[Size[0]];
            if (counts.ContainsKey(Size[1]))
                directory.MediumFiles = counts[Size[1]];
            if (counts.ContainsKey(Size[3]))
                directory.LargeFiles = counts[Size[3]];

            return Task.Run(() => directory);


        }
    }
}