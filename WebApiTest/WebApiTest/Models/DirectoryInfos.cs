﻿using System.Collections.Generic;
namespace WebApiTest.Models
{
    public class DirectoryInfos
    {       
        public List<Directory> FilesList { get; set; }
        public List<Directory> DirectoriesList { get; set; }
        public int SmallFiles { get; set; }
        public int MediumFiles { get; set; }
        public int LargeFiles { get; set; }
        public string Path { get; set; }        
    }
}