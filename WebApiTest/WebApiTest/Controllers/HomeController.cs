﻿using System;
using System.Web.Mvc;
using System.IO;
using System.Web.ModelBinding;
using WebApiTest.Models;

namespace WebApiTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult GetFileResult([QueryString] string path)
        {
            return path != null ? OpenFile(path) : View(new DirectoryInfos());
        }
        public ActionResult OpenFile(string path)
        {
            var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            var fileArray = new byte[fileStream.Length];
            IAsyncResult async = fileStream.BeginRead(fileArray, 0, fileArray.Length, null, null);
            fileStream.EndRead(async);
            fileStream.Close();
            return File(fileArray, path);
        }
    }
}
