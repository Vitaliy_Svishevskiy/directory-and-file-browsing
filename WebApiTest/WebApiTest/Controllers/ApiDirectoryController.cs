﻿using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using WebApiTest.Models;

namespace WebApiTest.Controllers
{
    public class ApiDirectoryController : ApiController
    {
        public async Task<DirectoryInfos> GetAsync(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return await DirectoryManager.Disks();
            }
            if (DirectoryManager.DriveInfos(path) == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return await DirectoryManager.DriveInfos(path);
        }
    }
}

