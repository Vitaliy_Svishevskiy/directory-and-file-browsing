﻿angular.module("dirBrowseApp")
    .controller("dirBrowse", ['$scope', 'Dir', function ($scope, dir) {
        $scope.path = "";
        $scope.open = function (p) {
            dir.get({ path: p || $scope.path }, function (data) {
                $scope.dir = data;
            }, function () {
            });
        };
        $scope.open();
    }]);