﻿angular.module("dirBrowseApp")
    .factory("Dir",['$resource',function ($resource) {
        return $resource("/api/apidirectory");
    }]);